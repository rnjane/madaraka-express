# Project Title

Madaraka Express Booking system

## Getting Started

The application runs in the command line interface. A working installation of python 2.7 is required.

### Prerequisites

Working installation of Python 2.7

### Installing

CLone https://github.com/rnjane/madaraka-express.git

run madaraka_functions.py

### Coding style 

pep8

## Built With

Python 2.7
Pickel 

## Authors

Robert Ndungu
Zack Mwangi
Sagini Abenga

## Acknowledgments

Esther Dama
