doc="""
Madaraka Express

Usage:
    madaraka_express view_passengers <admin> <password>
    madaraka_express update_bookings <admin> <password> <num_of_seats>
    madaraka_express book_seat <name> <id_num> <origin> <destination>
    madaraka_express view_available_seats
    madaraka_express view_departure_times

Examples:

"""
import cmd
from docopt import docopt, DocoptExit
from madraka_functions import view_passenger_manifest,update_bookings,book_seat,view_available_seats,view_departure_times

def doc_opt(f):
    """
    This decorator is used to simplify the try/except block and pass the result
    of the docopt parsing to the called action.
    """

    def fn(self, arg):
        """
        The DocoptExit is thrown when the args do not match.
        We print a message to the user and the usage block.
        The SystemExit exception prints the usage for --help
        """
        try:
            opt = docopt(fn.__doc__, arg)

        except DocoptExit as e:
            print('Invalid Command!')
            print(e)
            return

        except SystemExit:
            return

        return f(self, opt)

    fn.__name__ = f.__name__
    fn.__doc__ = f.__doc__
    fn.__dict__.update(f.__dict__)
    return fn


class InteractiveWeather(cmd.Cmd):
    intro = '\n' + ' ' \
            + '\n' + ' ' \
            + (' ' * 9 + '* ' * 10) + 'MADARAKA EXPRESS. BOOKING SYSTEM' + (' *' * 10) \
            + '\n' + ' ' \
            + '\n' + ' ' \
            + '\n' + ' ' \
            + '\n' + (' ' * 12 + ' ' * 23) + 'Type help for a list of commands' \
            + '\n' + ' ' \
            + '\n' + '- ' * 53

    prompt = '(madaraka_express>>)'

    @doc_opt
    def do_view_passenger_manifest(self,arg):
        """Usage: view_passenger_manifest <admin> <password>"""
        if arg['<admin>'] and arg['<password>']:
            view_passenger_manifest(arg['<admin>'],arg['<password>'])
    @doc_opt
    def do_update_bookings(self,arg):
        """Usage: update_bookings <admin> <password> <num_of_seats>"""
        if arg['<admin>'] and arg['<password>'] and ['<num_of_seats>']:
            update_bookings(arg['<admin>'],arg['<password>'],arg['<num_of_seats>'])

    @doc_opt
    def do_book_seat(self,arg):
        """Usage: book_seat <name> <id_num> <origin> <destination>"""
        if arg['<name>']and arg['<id_num>'] and arg['<origin>'] and arg['<destination>']:
            book_seat(arg['<name>'],arg['<id_num>'],arg['<origin>'],arg['<destination>'])

    @doc_opt
    def do_view_available_seats(self,arg):
        """Usage: view_available_seats"""
        view_available_seats()

    @doc_opt
    def do_view_departure_times(self,arg):
        """Usage: view_departure_times"""
        view_departure_times()






if __name__ == '__main__':
    try:
        InteractiveWeather().cmdloop()
    except KeyboardInterrupt:
        print('Please try again')
        exit()
