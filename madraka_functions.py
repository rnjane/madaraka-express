from pickle import load, dump
from os import remove


def view_passenger_manifest(admin, password):
    if admin == "admin" and password == "password":
        current_data = load(open('database.pkl', 'rb'))
        manifest = []
        for passenger in current_data[1:]:
            manifest.append(passenger['Name'])
        for name in manifest:
            print(name)
    else:
        print ("I'm sorry Hal I can't let you do that")


def update_bookings(admin, password, num_of_seats):
    if admin == "admin" and password == "password":
        current_data = load(open('database.pkl', 'rb'))
        current_data[0].update({'Remaining': num_of_seats})
        remove('database.pkl')
        dump(current_data, (open('database.pkl', 'wb')))
        print ("Sucess!Bookings updated remaining seats:", num_of_seats)


def book_seat(name, id_num, origin, destination):
    current_data = load(open('database.pkl', 'rb'))
    passenger = {'Name': name, 'ID_Number': id_num, 'Destination': destination, 'Origin': origin}
    current_data.append(passenger)
    remove('database.pkl')
    dump(current_data, (open('database.pkl', 'wb')))
    del current_data
    print('The Seat for:', passenger['Name'], 'Has Been Booked. Thank You For Using Our Services.')


def view_available_seats():
    data = load(open('database.pkl', 'rb'))
    remaining = data[0]['Capacity'] - (len(data) - 1)
    del data
    print('Remaining Seats:', remaining)


def view_departure_times():
    journeys = {'Journey J123': {'Destination': 'MSA', 'Departure Time': '2.00 p.m'},
                'Journey J124': {'Destination': 'NRB', 'Departure Time': '6.00 p.m'}}


    for journey in journeys:
        print (journey)
        for jtime in journeys[journey]:
            print (jtime, ',', journeys[journey][jtime])
