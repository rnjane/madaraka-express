from pickle import load,dump
from os import remove

def book_seat(name,id_num,origin,destination):
	current_data = load(open('database.pkl','rb'))
	passenger = {'Name':name,'ID_Number':id_num,'Destination':destination,'Origin':origin}
	current_data.append(passenger)
	remove('database.pkl')
	dump(current_data,(open('database.pkl','wb')))
	del current_data
	print('The Seat for:',passenger['Name'], 'Has Been Booked. Thank You For Using Our Services.')

def view_available_seats():
	data = load(open('database.pkl','rb'))
	remaining = data[0]['Capacity'] - (len(data) - 1)
	del data
	print('Remaining Seats:',remaining)

